{
	"contents": {
		"c86a2f0b-25ea-43c5-a099-a14da44911c4": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "businesspartner_flow",
			"subject": "BusinessPartner_Flow",
			"name": "BusinessPartner_Flow",
			"lastIds": "da02612c-3da5-4154-a2a6-5c2ae5cf8896",
			"events": {
				"f5bb9bcb-28e1-4950-8b40-3e8e4fd89449": {
					"name": "StartEvent1"
				},
				"573ec221-9c5a-4c54-b085-b683b7c40b9c": {
					"name": "EndEvent1"
				}
			},
			"activities": {
				"ff8cda42-4aaf-4753-874c-54aa2fde3a3f": {
					"name": "ExclusiveGateway1"
				},
				"b8794ae6-eba4-4a79-94cd-e14d721de1ca": {
					"name": "PUT-API_Business_Partner"
				}
			},
			"sequenceFlows": {
				"a23f56ab-ee1f-4c59-9e09-f89b7f94b3cd": {
					"name": "SequenceFlow3"
				},
				"140d34fa-d72f-4d0b-8e78-5be896bc3419": {
					"name": "SequenceFlow10"
				},
				"3f499832-100d-49dd-8b81-2f1a7e3a0b20": {
					"name": "SequenceFlow11"
				},
				"9fd1fbd5-8620-41bd-9ba2-a230c1ddd9dd": {
					"name": "SequenceFlow12"
				}
			},
			"diagrams": {
				"ff75c181-8a48-4400-a774-e59cd7807f58": {}
			}
		},
		"f5bb9bcb-28e1-4950-8b40-3e8e4fd89449": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1",
			"sampleContextRefs": {
				"e9c6f9b2-843a-4fc4-baae-9676336328e1": {}
			}
		},
		"573ec221-9c5a-4c54-b085-b683b7c40b9c": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"ff8cda42-4aaf-4753-874c-54aa2fde3a3f": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway1",
			"name": "ExclusiveGateway1",
			"default": "140d34fa-d72f-4d0b-8e78-5be896bc3419"
		},
		"b8794ae6-eba4-4a79-94cd-e14d721de1ca": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner",
			"httpMethod": "PUT",
			"xsrfPath": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner?$format=json",
			"requestVariable": "${context.request}",
			"responseVariable": "${context.response.data}",
			"id": "servicetask3",
			"name": "PUT-API_Business_Partner",
			"documentation": "Call SAP API \"API_Business_Partner\""
		},
		"a23f56ab-ee1f-4c59-9e09-f89b7f94b3cd": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "f5bb9bcb-28e1-4950-8b40-3e8e4fd89449",
			"targetRef": "ff8cda42-4aaf-4753-874c-54aa2fde3a3f"
		},
		"140d34fa-d72f-4d0b-8e78-5be896bc3419": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow10",
			"name": "SequenceFlow10",
			"sourceRef": "ff8cda42-4aaf-4753-874c-54aa2fde3a3f",
			"targetRef": "b8794ae6-eba4-4a79-94cd-e14d721de1ca"
		},
		"3f499832-100d-49dd-8b81-2f1a7e3a0b20": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow11",
			"name": "SequenceFlow11",
			"sourceRef": "b8794ae6-eba4-4a79-94cd-e14d721de1ca",
			"targetRef": "573ec221-9c5a-4c54-b085-b683b7c40b9c"
		},
		"9fd1fbd5-8620-41bd-9ba2-a230c1ddd9dd": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.action == false}",
			"id": "sequenceflow12",
			"name": "SequenceFlow12",
			"sourceRef": "ff8cda42-4aaf-4753-874c-54aa2fde3a3f",
			"targetRef": "573ec221-9c5a-4c54-b085-b683b7c40b9c"
		},
		"ff75c181-8a48-4400-a774-e59cd7807f58": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"f8de38dd-8340-446f-b690-9edc1a10b144": {},
				"0d49bc3f-d3cc-441f-aa7a-a9d1e1361511": {},
				"5bc6269a-b1db-458f-81ea-abd8fd53ea04": {},
				"9f9ba4c8-2e44-447d-b23d-255e014ce981": {},
				"6618f532-8146-40fe-b1ad-a875181810ee": {},
				"ca62946f-f157-4f20-96b9-148f80bf6bb3": {},
				"ae619f1e-bfe7-4f98-b979-865733f9ba56": {},
				"1bd2485f-fda9-4ed6-a367-f9771063805e": {}
			}
		},
		"e9c6f9b2-843a-4fc4-baae-9676336328e1": {
			"classDefinition": "com.sap.bpm.wfs.SampleContext",
			"reference": "/sample-data/BusinessPartner_Flow/initBP.json",
			"id": "default-start-context"
		},
		"f8de38dd-8340-446f-b690-9edc1a10b144": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 100,
			"y": 100,
			"width": 32,
			"height": 32,
			"object": "f5bb9bcb-28e1-4950-8b40-3e8e4fd89449"
		},
		"0d49bc3f-d3cc-441f-aa7a-a9d1e1361511": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 476,
			"y": 45,
			"width": 35,
			"height": 35,
			"object": "573ec221-9c5a-4c54-b085-b683b7c40b9c"
		},
		"5bc6269a-b1db-458f-81ea-abd8fd53ea04": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "116,118.5 228,118.5",
			"sourceSymbol": "f8de38dd-8340-446f-b690-9edc1a10b144",
			"targetSymbol": "9f9ba4c8-2e44-447d-b23d-255e014ce981",
			"object": "a23f56ab-ee1f-4c59-9e09-f89b7f94b3cd"
		},
		"9f9ba4c8-2e44-447d-b23d-255e014ce981": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 207,
			"y": 100,
			"object": "ff8cda42-4aaf-4753-874c-54aa2fde3a3f"
		},
		"6618f532-8146-40fe-b1ad-a875181810ee": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 441,
			"y": 224,
			"width": 100,
			"height": 60,
			"object": "b8794ae6-eba4-4a79-94cd-e14d721de1ca"
		},
		"ca62946f-f157-4f20-96b9-148f80bf6bb3": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "228,141.5 228,248 449,248",
			"sourceSymbol": "9f9ba4c8-2e44-447d-b23d-255e014ce981",
			"targetSymbol": "6618f532-8146-40fe-b1ad-a875181810ee",
			"object": "140d34fa-d72f-4d0b-8e78-5be896bc3419"
		},
		"ae619f1e-bfe7-4f98-b979-865733f9ba56": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "490.5,254 490.5,68",
			"sourceSymbol": "6618f532-8146-40fe-b1ad-a875181810ee",
			"targetSymbol": "0d49bc3f-d3cc-441f-aa7a-a9d1e1361511",
			"object": "3f499832-100d-49dd-8b81-2f1a7e3a0b20"
		},
		"1bd2485f-fda9-4ed6-a367-f9771063805e": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "228,100.5 228,58 487,58",
			"sourceSymbol": "9f9ba4c8-2e44-447d-b23d-255e014ce981",
			"targetSymbol": "0d49bc3f-d3cc-441f-aa7a-a9d1e1361511",
			"object": "9fd1fbd5-8620-41bd-9ba2-a230c1ddd9dd"
		},
		"da02612c-3da5-4154-a2a6-5c2ae5cf8896": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"sequenceflow": 12,
			"startevent": 1,
			"endevent": 1,
			"servicetask": 3,
			"exclusivegateway": 1
		}
	}
}