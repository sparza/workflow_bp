{
	"contents": {
		"e64ef604-3919-4006-b711-1f4bfac9c6e1": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "createbusinesspartner",
			"subject": "createBusinessPartner",
			"name": "createBusinessPartner",
			"documentation": "",
			"lastIds": "bba0ed94-27e6-4699-9b20-333a132534e2",
			"events": {
				"00bf23bc-1c8b-43d4-a2ee-bcdc0d50fa13": {
					"name": "StartEvent1"
				},
				"f7205f6d-23d0-42f0-9722-9bdef1b61609": {
					"name": "EndEvent1"
				}
			},
			"activities": {
				"3e1f24b4-d354-4b72-a0bb-d225ab82f194": {
					"name": "Create_BP"
				},
				"f897c4cc-4778-4f5e-9442-aac4fe0164f7": {
					"name": "ScriptTask2"
				},
				"05a1bcde-237e-420b-a211-90451fe0e9b6": {
					"name": "ExclusiveGateway1"
				}
			},
			"sequenceFlows": {
				"5db3b886-6174-4423-8b49-f235b16ba933": {
					"name": "SequenceFlow3"
				},
				"ebced03d-2d60-4141-a075-428027295df4": {
					"name": "SequenceFlow4"
				},
				"ea3cc5a8-8726-496d-8535-8fe35a512fae": {
					"name": "SequenceFlow5"
				},
				"68bbb1b6-fc20-41fc-b2de-a25bf30eeae8": {
					"name": "SequenceFlow6"
				},
				"09cad4f1-411b-4f34-9ecd-af5ca9f928ce": {
					"name": "SequenceFlow7"
				}
			},
			"diagrams": {
				"1bdf699e-51c2-43ca-a1af-158711db42f6": {}
			}
		},
		"00bf23bc-1c8b-43d4-a2ee-bcdc0d50fa13": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1",
			"sampleContextRefs": {
				"ae88076f-e457-4373-847f-c69b0459167e": {}
			}
		},
		"f7205f6d-23d0-42f0-9722-9bdef1b61609": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"3e1f24b4-d354-4b72-a0bb-d225ab82f194": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner",
			"httpMethod": "POST",
			"xsrfPath": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner?$format=json",
			"requestVariable": "${context.request}",
			"responseVariable": "${context.response.data}",
			"id": "servicetask1",
			"name": "Create_BP"
		},
		"5db3b886-6174-4423-8b49-f235b16ba933": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "3e1f24b4-d354-4b72-a0bb-d225ab82f194",
			"targetRef": "f7205f6d-23d0-42f0-9722-9bdef1b61609"
		},
		"1bdf699e-51c2-43ca-a1af-158711db42f6": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"89dea9c6-8682-4033-b286-e77bb605e882": {},
				"a6b4424a-c3ba-4399-88a3-1fa186675351": {},
				"d5324678-c408-4bca-97c6-4ce271438152": {},
				"13c30435-0c8a-4188-b3cd-2ad2d45e4f19": {},
				"b5bcbff2-14e7-4132-9124-dc5a53af466f": {},
				"5f8099af-c9ae-4b84-8e6b-c736a682e294": {},
				"5a2645f5-8aee-4adc-993d-20578d0e56f1": {},
				"ae9f42d5-2949-4b59-9d32-89f50d7adb5a": {},
				"2cf0a9c2-3d27-4108-aa10-e26703f21ed9": {},
				"2f668a43-f0e1-4ce8-8039-628941800285": {}
			}
		},
		"ae88076f-e457-4373-847f-c69b0459167e": {
			"classDefinition": "com.sap.bpm.wfs.SampleContext",
			"reference": "/sample-data/BusinessPartner_Flow/initBP.json",
			"id": "default-start-context"
		},
		"89dea9c6-8682-4033-b286-e77bb605e882": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 12,
			"y": 81,
			"width": 32,
			"height": 32,
			"object": "00bf23bc-1c8b-43d4-a2ee-bcdc0d50fa13"
		},
		"a6b4424a-c3ba-4399-88a3-1fa186675351": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 375.9999976158142,
			"y": 79.5,
			"width": 35,
			"height": 35,
			"object": "f7205f6d-23d0-42f0-9722-9bdef1b61609"
		},
		"d5324678-c408-4bca-97c6-4ce271438152": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 205.9999988079071,
			"y": 12,
			"width": 100,
			"height": 60,
			"object": "3e1f24b4-d354-4b72-a0bb-d225ab82f194"
		},
		"13c30435-0c8a-4188-b3cd-2ad2d45e4f19": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "305.9999988079071,42 340.99999821186066,42 340.99999821186066,97 375.9999976158142,97",
			"sourceSymbol": "d5324678-c408-4bca-97c6-4ce271438152",
			"targetSymbol": "a6b4424a-c3ba-4399-88a3-1fa186675351",
			"object": "5db3b886-6174-4423-8b49-f235b16ba933"
		},
		"bba0ed94-27e6-4699-9b20-333a132534e2": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"sequenceflow": 7,
			"startevent": 1,
			"endevent": 1,
			"servicetask": 1,
			"scripttask": 2,
			"exclusivegateway": 1
		},
		"f897c4cc-4778-4f5e-9442-aac4fe0164f7": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"reference": "/scripts/createBusinessPartner/createBPScript.js",
			"id": "scripttask2",
			"name": "ScriptTask2"
		},
		"b5bcbff2-14e7-4132-9124-dc5a53af466f": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 205.9999988079071,
			"y": 122,
			"width": 100,
			"height": 60,
			"object": "f897c4cc-4778-4f5e-9442-aac4fe0164f7"
		},
		"05a1bcde-237e-420b-a211-90451fe0e9b6": {
			"classDefinition": "com.sap.bpm.wfs.ExclusiveGateway",
			"id": "exclusivegateway1",
			"name": "ExclusiveGateway1",
			"default": "68bbb1b6-fc20-41fc-b2de-a25bf30eeae8"
		},
		"5f8099af-c9ae-4b84-8e6b-c736a682e294": {
			"classDefinition": "com.sap.bpm.wfs.ui.ExclusiveGatewaySymbol",
			"x": 94,
			"y": 76,
			"object": "05a1bcde-237e-420b-a211-90451fe0e9b6"
		},
		"ebced03d-2d60-4141-a075-428027295df4": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow4",
			"name": "SequenceFlow4",
			"sourceRef": "00bf23bc-1c8b-43d4-a2ee-bcdc0d50fa13",
			"targetRef": "05a1bcde-237e-420b-a211-90451fe0e9b6"
		},
		"5a2645f5-8aee-4adc-993d-20578d0e56f1": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "44,97 94,97",
			"sourceSymbol": "89dea9c6-8682-4033-b286-e77bb605e882",
			"targetSymbol": "5f8099af-c9ae-4b84-8e6b-c736a682e294",
			"object": "ebced03d-2d60-4141-a075-428027295df4"
		},
		"ea3cc5a8-8726-496d-8535-8fe35a512fae": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"condition": "${context.action == true}",
			"id": "sequenceflow5",
			"name": "SequenceFlow5",
			"sourceRef": "05a1bcde-237e-420b-a211-90451fe0e9b6",
			"targetRef": "3e1f24b4-d354-4b72-a0bb-d225ab82f194"
		},
		"ae9f42d5-2949-4b59-9d32-89f50d7adb5a": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "136,97 170.99999940395355,97 170.99999940395355,42 205.9999988079071,42",
			"sourceSymbol": "5f8099af-c9ae-4b84-8e6b-c736a682e294",
			"targetSymbol": "d5324678-c408-4bca-97c6-4ce271438152",
			"object": "ea3cc5a8-8726-496d-8535-8fe35a512fae"
		},
		"68bbb1b6-fc20-41fc-b2de-a25bf30eeae8": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow6",
			"name": "SequenceFlow6",
			"sourceRef": "05a1bcde-237e-420b-a211-90451fe0e9b6",
			"targetRef": "f897c4cc-4778-4f5e-9442-aac4fe0164f7"
		},
		"2cf0a9c2-3d27-4108-aa10-e26703f21ed9": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "136,97 170.99999940395355,97 170.99999940395355,152 205.9999988079071,152",
			"sourceSymbol": "5f8099af-c9ae-4b84-8e6b-c736a682e294",
			"targetSymbol": "b5bcbff2-14e7-4132-9124-dc5a53af466f",
			"object": "68bbb1b6-fc20-41fc-b2de-a25bf30eeae8"
		},
		"09cad4f1-411b-4f34-9ecd-af5ca9f928ce": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow7",
			"name": "SequenceFlow7",
			"sourceRef": "f897c4cc-4778-4f5e-9442-aac4fe0164f7",
			"targetRef": "f7205f6d-23d0-42f0-9722-9bdef1b61609"
		},
		"2f668a43-f0e1-4ce8-8039-628941800285": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "305.9999988079071,152 340.99999821186066,152 340.99999821186066,97 375.9999976158142,97",
			"sourceSymbol": "b5bcbff2-14e7-4132-9124-dc5a53af466f",
			"targetSymbol": "a6b4424a-c3ba-4399-88a3-1fa186675351",
			"object": "09cad4f1-411b-4f34-9ecd-af5ca9f928ce"
		}
	}
}