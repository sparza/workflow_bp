{
	"contents": {
		"2235e96a-35b9-42d7-beb4-9aa860ac8311": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "businesspartner_get",
			"subject": "BusinessPartner_Get",
			"name": "BusinessPartner_Get",
			"documentation": "",
			"lastIds": "398f8b0c-43e6-4700-8085-2d45e4c0956a",
			"events": {
				"a8b55a73-93d2-4c1c-a313-beb3b64cce1c": {
					"name": "StartEvent1"
				},
				"fd447558-c48b-4ad5-971d-0f9ec1d504a9": {
					"name": "EndEvent1"
				}
			},
			"activities": {
				"7e496638-1f97-4b6a-b455-b50e0698de98": {
					"name": "get_BusinessPartner"
				}
			},
			"sequenceFlows": {
				"166ebf30-8ac1-4271-bec8-1a45cbb84612": {
					"name": "SequenceFlow1"
				},
				"21b1414b-caf6-400b-be1c-bef34b3bd337": {
					"name": "SequenceFlow2"
				}
			},
			"diagrams": {
				"f0378fd4-0a7a-4392-aa19-dd05fb0c552f": {}
			}
		},
		"a8b55a73-93d2-4c1c-a313-beb3b64cce1c": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1"
		},
		"fd447558-c48b-4ad5-971d-0f9ec1d504a9": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"166ebf30-8ac1-4271-bec8-1a45cbb84612": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow1",
			"name": "SequenceFlow1",
			"sourceRef": "a8b55a73-93d2-4c1c-a313-beb3b64cce1c",
			"targetRef": "7e496638-1f97-4b6a-b455-b50e0698de98"
		},
		"f0378fd4-0a7a-4392-aa19-dd05fb0c552f": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"14ba81cf-893d-413e-ab9d-3802cec36434": {},
				"0d3ba0de-40be-4eb8-b52b-ab9f6930413f": {},
				"65161d0d-05a0-4b07-b3d5-d86ea79cbb7d": {},
				"de18b10b-59f4-492c-8c22-aed6a6160dc5": {},
				"d96e9fe1-6726-4915-b5ae-530932ca8601": {}
			}
		},
		"14ba81cf-893d-413e-ab9d-3802cec36434": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": 100,
			"y": 100,
			"width": 32,
			"height": 32,
			"object": "a8b55a73-93d2-4c1c-a313-beb3b64cce1c"
		},
		"0d3ba0de-40be-4eb8-b52b-ab9f6930413f": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 340,
			"y": 100,
			"width": 35,
			"height": 35,
			"object": "fd447558-c48b-4ad5-971d-0f9ec1d504a9"
		},
		"65161d0d-05a0-4b07-b3d5-d86ea79cbb7d": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "116,117 219,117",
			"sourceSymbol": "14ba81cf-893d-413e-ab9d-3802cec36434",
			"targetSymbol": "de18b10b-59f4-492c-8c22-aed6a6160dc5",
			"object": "166ebf30-8ac1-4271-bec8-1a45cbb84612"
		},
		"398f8b0c-43e6-4700-8085-2d45e4c0956a": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"sequenceflow": 2,
			"startevent": 1,
			"endevent": 1,
			"servicetask": 1
		},
		"7e496638-1f97-4b6a-b455-b50e0698de98": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "FS1",
			"path": "/sap/opu/odata/sap/API_BUSINESS_PARTNER/A_BusinessPartner('000082122')",
			"httpMethod": "GET",
			"responseVariable": "${context.response}",
			"id": "servicetask1",
			"name": "get_BusinessPartner"
		},
		"de18b10b-59f4-492c-8c22-aed6a6160dc5": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 169,
			"y": 88,
			"width": 100,
			"height": 60,
			"object": "7e496638-1f97-4b6a-b455-b50e0698de98"
		},
		"21b1414b-caf6-400b-be1c-bef34b3bd337": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow2",
			"name": "SequenceFlow2",
			"sourceRef": "7e496638-1f97-4b6a-b455-b50e0698de98",
			"targetRef": "fd447558-c48b-4ad5-971d-0f9ec1d504a9"
		},
		"d96e9fe1-6726-4915-b5ae-530932ca8601": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "219,117.75 357.5,117.75",
			"sourceSymbol": "de18b10b-59f4-492c-8c22-aed6a6160dc5",
			"targetSymbol": "0d3ba0de-40be-4eb8-b52b-ab9f6930413f",
			"object": "21b1414b-caf6-400b-be1c-bef34b3bd337"
		}
	}
}